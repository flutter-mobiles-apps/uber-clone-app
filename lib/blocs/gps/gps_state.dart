part of 'gps_bloc.dart';

class GpsState extends Equatable {
  const GpsState({
    required this.isGpsEnable,
    required this.isGpsPermissionGranted,
  });

  final bool isGpsEnable;
  final bool isGpsPermissionGranted;

  GpsState copyWith({
    bool? isGpsEnable,
    bool? isGpsPermissionGranted,
  }) =>
      GpsState(
        isGpsEnable: isGpsEnable ?? this.isGpsEnable,
        isGpsPermissionGranted:
            isGpsPermissionGranted ?? this.isGpsPermissionGranted,
      );

  @override
  List<Object> get props => [isGpsEnable, isGpsPermissionGranted];

  @override
  String toString() =>
      '{ isGpsState: $isGpsEnable, isGpsPermissionGranted: $isGpsPermissionGranted }';
}
