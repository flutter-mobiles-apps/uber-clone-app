import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uber_clone_app/blocs/gps/gps_bloc.dart';
import 'package:uber_clone_app/screens/screens.dart';

void main() {
  runApp(
    MultiBlocProvider(
      providers: [
        // Provider of GPS
        BlocProvider(
          create: (_) => GpsBloc(),
        ),
        //
      ],
      child: const MapsApp(),
    ),
  );
}

//=> runApp(const MapsApp());

class MapsApp extends StatelessWidget {
  const MapsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'MapsApp',
      debugShowCheckedModeBanner: false,
      home: GpsAccessScreen(),
    );
  }
}
